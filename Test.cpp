// Entry point for testing

#include <iostream>

#include "doctest.h"

#include "sources/MagicalContainer.hpp"


using namespace doctest;
using namespace ariel;

using AscendingIterator = MagicalContainer::AscendingIterator;
using SideCrossIterator = MagicalContainer::SideCrossIterator;
using PrimeIterator = MagicalContainer::PrimeIterator;

TEST_SUITE("Magical Container Unit Test")
{
    TEST_CASE("build")
    {
        MagicalContainer container;

        CHECK_EQ(container.size(), 0);

    }

    TEST_CASE("add")
    {
        DEBUG("add");

        MagicalContainer container;

        container.addElement(5);
        container.addElement(4);
        container.addElement(6);
        container.addElement(3);
        container.addElement(7);
        container.addElement(2);
        container.addElement(8);
        container.addElement(1);

        CHECK_EQ(container.size(), 8);
    }

    TEST_CASE("remove")
    {
        DEBUG("remove");

        MagicalContainer container;

        container.addElement(5);
        container.addElement(2);
        container.addElement(3);

        CHECK_EQ(container.size(), 3);

        container.removeElement(2);

        CHECK_EQ(container.size(), 2);

        container.addElement(0);
        container.addElement(1);

        CHECK_EQ(container.size(), 4);

        container.removeElement(0);
        container.removeElement(5);
        container.removeElement(3);

        CHECK_EQ(container.size(), 1);

        container.removeElement(1);

        CHECK_EQ(container.size(), 0);
    }

    TEST_CASE("copy and move")
    {
        SUBCASE("Case 1")
        {
            DEBUG("CASE 1");

            MagicalContainer container1;
            container1.addElement(1);
            container1.addElement(2);
            container1.addElement(3);
            container1.addElement(4);

            CHECK_EQ(container1.size(), 4);

            MagicalContainer container2(MagicalContainer{});
            container2.addElement(1);
            container2.addElement(2);
            container2.addElement(3);
            container2.addElement(4);

            CHECK_EQ(container2.size(), 4);

            MagicalContainer container3(container1);

            CHECK_EQ(container3.size(), 4);

            container1.removeElement(4);

            CHECK_EQ(container1.size(), 3);
            CHECK_EQ(container3.size(), 4);

            container3.removeElement(1);
            container3.removeElement(2);

            CHECK_EQ(container1.size(), 3);
            CHECK_EQ(container3.size(), 2);
        }

        SUBCASE("Case 2")
        {
            DEBUG("CASE 2");

            MagicalContainer container1;

            for (int i = 0; i < 3; ++i) {
                MagicalContainer container2;
                container2.addElement(1);
                container2.addElement(2);
                container2.addElement(3);
                container2.addElement(4);

                container1 = container2;

                CHECK_EQ(container1.size(), 4);
                CHECK_EQ(container2.size(), 4);
            }

            CHECK_EQ(container1.size(), 4);
        }
    }
}

TEST_CASE("build")
{
    MagicalContainer container;
    container.addElement(1);
    container.addElement(4);
    container.addElement(2);
    container.addElement(3);

    SUBCASE("Ascending Iterator") {
        AscendingIterator it1;
        AscendingIterator it2(container);
        AscendingIterator it3 = it2.begin();
        AscendingIterator it4 = AscendingIterator(container).begin();
        AscendingIterator it5 = it4;
        AscendingIterator it6 = it1;

        CHECK(it1 == it1.end());
        CHECK(it2 != it2.end());
        CHECK(it3 == it2.begin());
        CHECK(it4 == it2.begin());
        CHECK(it5 == it2.begin());
        CHECK_THROWS(it1.begin());
        CHECK(it6 != it2.begin());
        CHECK(it6 == it1.end());
        CHECK(it6 == it2.end());
    }

    SUBCASE("SideCross Iterator") {
        SideCrossIterator it1;
        SideCrossIterator it2(container);
        SideCrossIterator it3 = it2.begin();
        SideCrossIterator it4 = SideCrossIterator(container).begin();
        SideCrossIterator it5 = it4;
        SideCrossIterator it6 = it1;

        CHECK(it1 == it1.end());
        CHECK(it2 != it2.end());
        CHECK(it3 == it2.begin());
        CHECK(it4 == it2.begin());
        CHECK(it5 == it2.begin());
        CHECK_THROWS(it1.begin());
        CHECK(it6 != it2.begin());
        CHECK(it6 == it1.end());
        CHECK(it6 == it2.end());
    }

    SUBCASE("Prime Iterator") {
        PrimeIterator it1;
        PrimeIterator it2(container);
        PrimeIterator it3 = it2.begin();
        PrimeIterator it4 = PrimeIterator(container).begin();
        PrimeIterator it5 = it4;
        PrimeIterator it6 = it1;

        CHECK(it1 == it1.end());
        CHECK(it2 != it2.end());
        CHECK(it3 == it2.begin());
        CHECK(it4 == it2.begin());
        CHECK(it5 == it2.begin());
        CHECK_THROWS(it1.begin());
        CHECK(it6 != it2.begin());
        CHECK(it6 == it1.end());
        CHECK(it6 == it2.end());
    }
}

TEST_SUITE("Ascending Iterator") {
    TEST_CASE("compere") {
        MagicalContainer container;
        container.addElement(1);
        container.addElement(4);
        container.addElement(2);
        container.addElement(3);

        AscendingIterator it1(container);
        AscendingIterator it2(container);

        CHECK(it1 == it2);
        CHECK(++it1 == ++it2);
        CHECK(it1 <= it2);
        CHECK(it1 >= it2);

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));
    }

    TEST_CASE("values") {
        MagicalContainer container;
        container.addElement(1);
        container.addElement(4);
        container.addElement(2);
        container.addElement(3);

        AscendingIterator it(container);

        CHECK_EQ(*it, 1);
        CHECK_EQ(*it, 1);
        ++it;
        CHECK_EQ(*it, 2);
        ++it;
        CHECK_EQ(*it, 3);
        CHECK_EQ(*(++it), 4);
        ++it;

        CHECK_THROWS(*it);
        CHECK_THROWS(++it);
    }

    TEST_CASE("changes") {
        MagicalContainer container;
        container.addElement(2);
        container.addElement(5);
        container.addElement(19);
        container.addElement(21);

        AscendingIterator it(container);
        CHECK_EQ(*it, 2);

        ++it;
        CHECK_EQ(*it, 5);

        container.addElement(3);

        CHECK_EQ(*it, 5);

        ++it;
        CHECK_EQ(*it, 19);

        container.addElement(4);
        container.addElement(7);
        container.addElement(6);
        container.addElement(8);

        CHECK_EQ(*it, 19);
    }
}

TEST_SUITE("SideCross Iterator") {
    TEST_CASE("compere") {
        MagicalContainer container;
        container.addElement(1);
        container.addElement(4);
        container.addElement(2);
        container.addElement(3);

        PrimeIterator it1(container);
        PrimeIterator it2(container);

        CHECK(it1 == it2);
        CHECK(++it1 == ++it2);
        CHECK(it1 <= it2);
        CHECK(it1 >= it2);

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));
    }

    TEST_CASE("values") {
        MagicalContainer container;
        container.addElement(2);
        container.addElement(4);
        container.addElement(1);
        container.addElement(3);

        PrimeIterator it(container);

        CHECK_EQ(*it, 1);
        CHECK_EQ(*it, 1);
        ++it;
        CHECK_EQ(*it, 4);
        ++it;
        CHECK_EQ(*it, 2);
        CHECK_EQ(*(++it), 3);
        ++it;

        CHECK_THROWS(*it);
        CHECK_THROWS(++it);
    }

    TEST_CASE("changes") {
        MagicalContainer container;
        container.addElement(2);
        container.addElement(3);
        container.addElement(4);
        container.addElement(5);

        PrimeIterator it(container);
        CHECK_EQ(*it, 2);

        ++it;
        CHECK_EQ(*it, 5);

        container.addElement(1);
        container.addElement(6);

        CHECK_EQ(*it, 5);

        ++it;
        CHECK_EQ(*it, 3);

        ++it;
        CHECK_EQ(*it, 4);
    }
}

TEST_SUITE("Prime Iterator") {
    TEST_CASE("compere") {
        MagicalContainer container;
        container.addElement(1);
        container.addElement(4);
        container.addElement(2);
        container.addElement(3);

        PrimeIterator it1(container);
        PrimeIterator it2(container);

        CHECK(it1 == it2);
        CHECK(++it1 == ++it2);
        CHECK(it1 <= it2);
        CHECK(it1 >= it2);

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));

        ++it1;

        CHECK(it1 > it2);
        CHECK(it2 < it1);
        CHECK(!(it1 < it2));
        CHECK(!(it2 > it1));
    }

    TEST_CASE("values") {
        MagicalContainer container;
        container.addElement(1);
        container.addElement(4);
        container.addElement(2);
        container.addElement(3);

        PrimeIterator it(container);

        CHECK_EQ(*it, 1);
        CHECK_EQ(*it, 1);
        ++it;
        CHECK_EQ(*it, 2);
        ++it;
        CHECK_EQ(*it, 3);
        CHECK_EQ(*(++it), 4);
        ++it;

        CHECK_THROWS(*it);
        CHECK_THROWS(++it);
    }

    TEST_CASE("changes") {
        MagicalContainer container;
        container.addElement(2);
        container.addElement(5);
        container.addElement(19);
        container.addElement(21);

        PrimeIterator it(container);
        CHECK_EQ(*it, 2);

        ++it;
        CHECK_EQ(*it, 5);

        container.addElement(3);

        CHECK_EQ(*it, 5);

        ++it;
        CHECK_EQ(*it, 19);

        container.addElement(4);
        container.addElement(7);
        container.addElement(6);
        container.addElement(8);

        CHECK_EQ(*it, 19);
    }
}
