#include "MagicalContainer.hpp"

#include <cmath>
#include <stdexcept>

namespace ariel {
    // utils
    static bool isPrime(int number) {
        if (number < 2) {
            return false;
        }
        if (number < 4) {
            return true;
        }

        int n = ((int) std::sqrt((long double) number)) + 1;

        for (int i = 2; i <= n; ++i) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    // public
    MagicalContainer::MagicalContainer()
            : m_first(nullptr), m_last(nullptr), m_firstPrime(nullptr), m_size(0) {

    }

    MagicalContainer::MagicalContainer(const MagicalContainer &other)
            : m_first(nullptr), m_last(nullptr), m_firstPrime(nullptr), m_size(0) {
        this->clone(other);
    }

    MagicalContainer::MagicalContainer(MagicalContainer &&other) noexcept
            : m_first(other.m_first), m_last(other.m_last), m_firstPrime(other.m_firstPrime), m_size(other.m_size) {

    }

    MagicalContainer::~MagicalContainer() {
        this->clear();
    }

    MagicalContainer &MagicalContainer::operator=(const MagicalContainer &other) {
        this->clone(other);
        return *this;
    }

    MagicalContainer &MagicalContainer::operator=(MagicalContainer &&other) noexcept {
        m_first = other.m_first;
        m_last = other.m_last;
        m_firstPrime = other.m_firstPrime;
        return *this;
    }

    void MagicalContainer::clone(const MagicalContainer &other) {

        this->clear();

        if (other.m_first == nullptr) {
            return;
        }

        Element *el = other.m_first;

        m_first = new Element(el->value, el->isPrime);
        m_size++;
        if (m_first->isPrime) {
            m_firstPrime = m_first;
        }

        Element *lastElement = m_first;
        Element *lastPrime = m_firstPrime;

        el = other.m_first->next;

        while (el != nullptr) {
            Element *newEl = new Element(el->value, el->isPrime);
            m_size++;
            lastElement->next = newEl;
            newEl->prev = lastElement;
            if (newEl->isPrime) {
                if (lastPrime == nullptr) {
                    m_firstPrime = newEl;
                } else {
                    lastPrime->nextPrime = newEl;
                    newEl->prevPrime = lastPrime;
                }
                lastPrime = newEl;
            }
            lastElement = newEl;
            el = el->next;
        }
        m_last = lastElement;
    }

    void MagicalContainer::clear()
    {
        Element *element = m_first;
        while (element != nullptr) {
            Element *now = element;
            element = element->next;
            delete now;
        }

        m_first = nullptr;
        m_last = nullptr;
        m_firstPrime = nullptr;

        m_size = 0;
    }

    int MagicalContainer::size() const {
        return m_size;
    }

    void MagicalContainer::addElement(int value) {
        bool prime = isPrime(value);

        Element *newElement = new Element(value, prime);
        m_size++;

        if (m_size == 1) {
            m_first = newElement;
            m_last = newElement;
            if (prime) {
                m_firstPrime = newElement;
            }
            return;
        }

        if (value < m_first->value) {
            m_first->prev = newElement;
            newElement->next = m_first;
            m_first = newElement;
            if (prime) {
                if (m_firstPrime != nullptr) {
                    m_firstPrime->prevPrime = newElement;
                    newElement->nextPrime = m_firstPrime;
                }
                m_firstPrime = newElement;
            }
            return;
        }

        // set on normal list
        Element *elementAfter = m_first;
        while (elementAfter != nullptr && elementAfter->value < value) {
            elementAfter = elementAfter->next;
        }

        if (elementAfter == nullptr) {
            newElement->prev = m_last;
            m_last->next = newElement;
            m_last = newElement;
        } else {
            if (elementAfter->value == value) {
                delete newElement;
                m_size--;
                return;
            }
            Element *elementBefore = elementAfter->prev;
            elementBefore->next = newElement;
            newElement->prev = elementBefore;
            newElement->next = elementAfter;
            elementAfter->prev = newElement;
        }

        // set on prime list
        if (prime) {
            if (m_firstPrime == nullptr) {
                m_firstPrime = newElement;
            } else if (value < m_firstPrime->value) {
                newElement->nextPrime = m_firstPrime;
                m_firstPrime->prevPrime = newElement;
                m_firstPrime = newElement;
            } else {
                Element *elementBefore = m_firstPrime;
                elementAfter = m_firstPrime->nextPrime;
                while (elementAfter != nullptr && elementAfter->value < value) {
                    elementBefore = elementAfter;
                    elementAfter = elementAfter->nextPrime;
                }

                if (elementAfter == nullptr) {
                    elementBefore->nextPrime = newElement;
                    newElement->prevPrime = elementBefore;
                } else {
                    elementBefore->nextPrime = newElement;
                    newElement->prevPrime = elementBefore;
                    newElement->nextPrime = elementAfter;
                    elementAfter->prevPrime = newElement;
                }
            }
        }
    }

    void MagicalContainer::removeElement(int value) {
        Element *element = m_first;
        while (element != nullptr && element->value != value) {
            element = element->next;
        }

        // not found
        if (element == nullptr) {
            throw std::runtime_error("Not found! why?");
        }

        if (m_size == 1) {
            m_firstPrime = nullptr;
            m_first = nullptr;
            m_last = nullptr;
            delete element;
            m_size--;
            return;
        }

        // remove from prime list
        if (element->isPrime) {
            if (element == m_firstPrime) {
                m_firstPrime = m_firstPrime->nextPrime;
                if (m_firstPrime != nullptr) {
                    m_firstPrime->prevPrime = nullptr;
                }
            } else {
                Element *primeBefore = element->prevPrime;
                Element *primeAfter = element->nextPrime;
                primeBefore->nextPrime = primeAfter;
                if (primeAfter != nullptr) {
                    primeAfter->prevPrime = primeBefore;
                }
            }
        }

        // remove from normal list
        if (element == m_first) {
            m_first = m_first->next;
            m_first->prev = nullptr;
        } else if (element == m_last) {
            m_last = m_last->prev;
            m_last->next = nullptr;
        } else {
            Element *elementBefore = element->prev;
            Element *elementAfter = element->next;
            elementBefore->next = elementAfter;
            elementAfter->prev = elementBefore;
        }

        m_size--;
        delete element;
    }

    MagicalContainer::AscendingIterator::AscendingIterator()
            : m_containerFirst(nullptr), m_current(nullptr) {

    }

    MagicalContainer::AscendingIterator::AscendingIterator(const MagicalContainer &container)
            : m_containerFirst(&container.m_first), m_current(container.m_first) {

    }

    MagicalContainer::AscendingIterator::AscendingIterator(const Element *const *containerFirst, Element *current)
            : m_containerFirst(containerFirst), m_current(current) {

    }

    MagicalContainer::AscendingIterator::AscendingIterator(const AscendingIterator &other)
            : m_containerFirst(other.m_containerFirst), m_current(other.m_current) {
    }

    MagicalContainer::AscendingIterator::AscendingIterator(AscendingIterator &&other) noexcept
            : m_containerFirst(other.m_containerFirst), m_current(other.m_current) {
    }

    MagicalContainer::AscendingIterator &MagicalContainer::AscendingIterator::operator=(const AscendingIterator &other) {
        if (m_containerFirst != other.m_containerFirst) {
            throw std::runtime_error("Is not the same container!");
        }

        AscendingIterator it(other);
        this->m_containerFirst = it.m_containerFirst;
        this->m_current = it.m_current;
        return *this;
    }

    MagicalContainer::AscendingIterator &MagicalContainer::AscendingIterator::operator=(AscendingIterator &&other) noexcept {
        this->m_containerFirst = other.m_containerFirst;
        this->m_current = other.m_current;
        return *this;
    }

    MagicalContainer::AscendingIterator MagicalContainer::AscendingIterator::begin() const {
        if (m_containerFirst == nullptr) {
            throw std::runtime_error("null pointer error!");
        }
        return MagicalContainer::AscendingIterator(m_containerFirst, (Element *) *m_containerFirst);
    }

    MagicalContainer::AscendingIterator MagicalContainer::AscendingIterator::end() const {
        return MagicalContainer::AscendingIterator();
    }

    MagicalContainer::AscendingIterator &MagicalContainer::AscendingIterator::operator++() {
        if (m_current == nullptr) {
            throw std::runtime_error("This iterator is ended!");
        }

        m_current = m_current->next;
        return *this;
    }

    int MagicalContainer::AscendingIterator::operator*() const {
        if (m_current == nullptr) {
            throw std::runtime_error("This iterator is ended!");
        }

        return m_current->value;
    }

    bool MagicalContainer::AscendingIterator::operator==(const AscendingIterator &other) const {
        return m_current == other.m_current;
    }

    bool MagicalContainer::AscendingIterator::operator<(const AscendingIterator &other) const {
        if (m_containerFirst != other.m_containerFirst) {
            return false;
        }

        if (m_current == nullptr) {
            return false;
        }

        if (other.m_current == nullptr) {
            return true;
        }

        return m_current->value < other.m_current->value;
    }

    MagicalContainer::SideCrossIterator::SideCrossIterator()
            : m_containerFirst(nullptr), m_containerLast(nullptr), m_currentForward(nullptr),
              m_currentBackward(nullptr), m_moveForward(true) {

    }

    MagicalContainer::SideCrossIterator::SideCrossIterator(const MagicalContainer &container)
            : m_containerFirst(&container.m_first), m_containerLast(&container.m_last),
              m_currentForward(container.m_first), m_currentBackward(container.m_last), m_moveForward(true) {
    }

    MagicalContainer::SideCrossIterator::SideCrossIterator(const Element *const *containerFirst,
                                                           const Element *const *containerLast,
                                                           Element *currentForward, Element *currentBackward)
            : m_containerFirst(containerFirst), m_containerLast(containerLast), m_currentForward(currentForward),
              m_currentBackward(currentBackward), m_moveForward(true) {
    }

    MagicalContainer::SideCrossIterator::SideCrossIterator(const SideCrossIterator &other)
            : m_containerFirst(other.m_containerFirst), m_containerLast(other.m_containerLast),
              m_currentForward(other.m_currentForward), m_currentBackward(other.m_currentBackward) {
    }

    MagicalContainer::SideCrossIterator::SideCrossIterator(SideCrossIterator &&other) noexcept
            : m_containerFirst(other.m_containerFirst), m_containerLast(other.m_containerLast),
              m_currentForward(other.m_currentForward), m_currentBackward(other.m_currentBackward) {
    }

    MagicalContainer::SideCrossIterator &MagicalContainer::SideCrossIterator::operator=(const SideCrossIterator &other) {
        if (m_containerFirst != other.m_containerFirst) {
            throw std::runtime_error("Is not the same container!");
        }

        SideCrossIterator it(other);
        this->m_containerFirst = it.m_containerFirst;
        this->m_containerLast = it.m_containerLast;
        this->m_currentForward = it.m_currentForward;
        this->m_currentBackward = it.m_currentBackward;
        return *this;
    }

    MagicalContainer::SideCrossIterator &MagicalContainer::SideCrossIterator::operator=(SideCrossIterator &&other) noexcept {
        this->m_containerFirst = other.m_containerFirst;
        this->m_containerLast = other.m_containerLast;
        this->m_currentForward = other.m_currentForward;
        this->m_currentBackward = other.m_currentBackward;
        return *this;
    }

    MagicalContainer::SideCrossIterator MagicalContainer::SideCrossIterator::begin() const {
        if (m_containerFirst == nullptr) {
            throw std::runtime_error("null pointer error!");
        }

        return MagicalContainer::SideCrossIterator(m_containerFirst, m_containerLast, (Element *) *m_containerFirst,
                                                   (Element *) *m_containerLast);
    }

    MagicalContainer::SideCrossIterator MagicalContainer::SideCrossIterator::end() const {
        return MagicalContainer::SideCrossIterator(m_containerFirst, m_containerLast, nullptr, nullptr);
    }

    MagicalContainer::SideCrossIterator &MagicalContainer::SideCrossIterator::operator++() {
        if (m_containerFirst == nullptr || m_currentBackward == nullptr) {
            throw std::runtime_error("ended!");
        }

        if (m_currentForward == m_currentBackward) {
            m_currentForward = nullptr;
            m_currentBackward = nullptr;
            return *this;
        }

        if (m_moveForward) {
            m_currentForward = m_currentForward->next;
        } else {
            m_currentBackward = m_currentBackward->prev;
        }

        m_moveForward = !m_moveForward;

        return *this;
    }

    int MagicalContainer::SideCrossIterator::operator*() const {
        if (m_containerFirst == nullptr || m_currentBackward == nullptr) {
            throw std::runtime_error("ended!");
        }

        if (m_moveForward) {
            return m_currentForward->value;
        }
        return m_currentBackward->value;
    }

    bool MagicalContainer::SideCrossIterator::operator==(const SideCrossIterator &other) const {
        if (m_containerFirst != other.m_containerFirst || m_containerLast != other.m_containerLast) {
            return false;
        }

        return m_currentForward == other.m_currentForward && m_currentBackward == other.m_currentBackward;
    }

    bool MagicalContainer::SideCrossIterator::operator<(const SideCrossIterator &other) const {
        if (m_containerFirst != other.m_containerFirst || m_containerLast != other.m_containerLast) {
            return false;
        }

        if (m_currentForward == nullptr) {
            return false;
        }

        if (other.m_currentForward == nullptr) {
            return true;
        }

        if (m_currentBackward->value == other.m_currentBackward->value) {
            return m_currentForward->value < other.m_currentForward->value;
        }

        return m_currentBackward->value > other.m_currentBackward->value;
    }

    MagicalContainer::PrimeIterator::PrimeIterator()
            : m_containerFirstPrime(nullptr), m_current(nullptr) {

    }

    MagicalContainer::PrimeIterator::PrimeIterator(const MagicalContainer &container)
            : m_containerFirstPrime(&container.m_firstPrime), m_current(container.m_firstPrime) {

    }

    MagicalContainer::PrimeIterator::PrimeIterator(const Element *const *containerFirstPrime, Element *current)
            : m_containerFirstPrime(containerFirstPrime), m_current(current) {

    }

    MagicalContainer::PrimeIterator::PrimeIterator(const PrimeIterator &other)
            : m_containerFirstPrime(other.m_containerFirstPrime), m_current(other.m_current) {
    }

    MagicalContainer::PrimeIterator::PrimeIterator(PrimeIterator &&other) noexcept
            : m_containerFirstPrime(other.m_containerFirstPrime), m_current(other.m_current) {
    }

    MagicalContainer::PrimeIterator &MagicalContainer::PrimeIterator::operator=(const PrimeIterator &other) {
        if (m_containerFirstPrime != other.m_containerFirstPrime) {
            throw std::runtime_error("Is not the same container!");
        }

        PrimeIterator it(other);
        this->m_containerFirstPrime = it.m_containerFirstPrime;
        this->m_current = it.m_current;
        return *this;
    }

    MagicalContainer::PrimeIterator &MagicalContainer::PrimeIterator::operator=(PrimeIterator &&other) noexcept {
        this->m_containerFirstPrime = other.m_containerFirstPrime;
        this->m_current = other.m_current;
        return *this;
    }

    MagicalContainer::PrimeIterator MagicalContainer::PrimeIterator::begin() const {
        if (m_containerFirstPrime == nullptr) {
            throw std::runtime_error("null pointer error!");
        }

        return MagicalContainer::PrimeIterator(m_containerFirstPrime, (Element *) *m_containerFirstPrime);
    }

    MagicalContainer::PrimeIterator MagicalContainer::PrimeIterator::end() const {
        return MagicalContainer::PrimeIterator();
    }

    MagicalContainer::PrimeIterator &MagicalContainer::PrimeIterator::operator++() {
        if (m_current == nullptr) {
            throw std::runtime_error("This iterator is ended!");
        }

        m_current = m_current->nextPrime;
        return *this;
    }

    int MagicalContainer::PrimeIterator::operator*() const {
        if (m_current == nullptr) {
            throw std::runtime_error("This iterator is ended!");
        }

        return m_current->value;
    }

    bool MagicalContainer::PrimeIterator::operator==(const PrimeIterator &other) const {
        return m_current == other.m_current;
    }

    bool MagicalContainer::PrimeIterator::operator<(const PrimeIterator &other) const {
        if (m_containerFirstPrime != other.m_containerFirstPrime) {
            return false;
        }

        if (m_current == nullptr) {
            return false;
        }

        if (other.m_current == nullptr) {
            return true;
        }


        return m_current->value < other.m_current->value;
    }
}
