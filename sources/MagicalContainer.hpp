#pragma once

#include <vector>

#include "build.h"

namespace ariel {

    class Element {
    public:
        const int value;
        const bool isPrime;
        Element *next = nullptr;
        Element *prev = nullptr;
        Element *nextPrime = nullptr;
        Element *prevPrime = nullptr;

        Element();

        Element(int value, bool isPrime) : value(value), isPrime(isPrime) {}
    };

    class MagicalContainer {
    public:
        class AscendingIterator {
        private:
            const Element *const *m_containerFirst;
            Element *m_current;

        public:
            AscendingIterator();

            AscendingIterator(const MagicalContainer &container);

        private:
            AscendingIterator(const Element *const * containerFirst, Element* current);

        public:
            AscendingIterator(const AscendingIterator& other);

            AscendingIterator(AscendingIterator&& other) noexcept;

            virtual ~AscendingIterator() { }

            AscendingIterator& operator=(const AscendingIterator& other);

            AscendingIterator& operator=(AscendingIterator&& other) noexcept;

            AscendingIterator begin() const;

            AscendingIterator end() const;

            AscendingIterator &operator++();

            int operator*() const;

            bool operator==(const AscendingIterator &other) const;

            bool operator!=(const AscendingIterator &other) const { return !(*this == other); }

            bool operator<(const AscendingIterator &other) const;

            bool operator<=(const AscendingIterator &other) const { return *this == other || *this < other; }

            bool operator>(const AscendingIterator &other) const { return other < *this; }

            bool operator>=(const AscendingIterator &other) const { return other <= *this; }
        };

        class SideCrossIterator {
        private:
            const Element *const *m_containerFirst;
            const Element *const *m_containerLast;

            // if m_currentForward and m_currentBackward are null then it ended!
            Element *m_currentForward;
            Element *m_currentBackward;
            bool m_moveForward;

        public:
            SideCrossIterator();

            SideCrossIterator(const MagicalContainer &container);

        private:
            SideCrossIterator(const Element *const * containerFirst, const Element *const * containerLast, Element* currentForward, Element* currentBackward);

        public:
            SideCrossIterator(const SideCrossIterator& other);

            SideCrossIterator(SideCrossIterator&& other) noexcept;

            virtual ~SideCrossIterator() { }

            SideCrossIterator& operator=(const SideCrossIterator& other);

            SideCrossIterator& operator=(SideCrossIterator&& other) noexcept;

            SideCrossIterator begin() const;

            SideCrossIterator end() const;

            SideCrossIterator &operator++();

            int operator*() const;

            bool operator==(const SideCrossIterator &other) const;

            bool operator!=(const SideCrossIterator &other) const { return !(*this == other); }

            bool operator<(const SideCrossIterator &other) const;

            bool operator<=(const SideCrossIterator &other) const { return *this == other || *this < other; }

            bool operator>(const SideCrossIterator &other) const { return other < *this; }

            bool operator>=(const SideCrossIterator &other) const { return other <= *this; }
        };

        class PrimeIterator {
        private:
            const Element *const *m_containerFirstPrime;
            Element *m_current;

        public:
            PrimeIterator();

            PrimeIterator(const MagicalContainer &container);

        private:
            PrimeIterator(const Element *const * containerFirstPrime, Element* current);

        public:
            PrimeIterator(const PrimeIterator& other);

            PrimeIterator(PrimeIterator&& other) noexcept;

            virtual ~PrimeIterator() { }

            PrimeIterator& operator=(const PrimeIterator& other);

            PrimeIterator& operator=(PrimeIterator&& other) noexcept;

            PrimeIterator begin() const;

            PrimeIterator end() const;

            PrimeIterator &operator++();

            int operator*() const;

            bool operator==(const PrimeIterator &other) const;

            bool operator!=(const PrimeIterator &other) const { return !(*this == other); }

            bool operator<(const PrimeIterator &other) const;

            bool operator<=(const PrimeIterator &other) const { return *this == other || *this < other; }

            bool operator>(const PrimeIterator &other) const { return other < *this; }

            bool operator>=(const PrimeIterator &other) const { return other <= *this; }
        };

    private:
        Element *m_first;
        Element *m_last;
        Element *m_firstPrime;
        int m_size;

    public:
        MagicalContainer();

        MagicalContainer(const MagicalContainer &other);

        MagicalContainer(MagicalContainer &&other) noexcept;

        virtual ~MagicalContainer();

        MagicalContainer& operator=(const MagicalContainer& other);

        MagicalContainer& operator=(MagicalContainer&& other) noexcept;

        int size() const;

        void addElement(int value);

        void removeElement(int value);

    private:
        void clone(const MagicalContainer &other);

        void clear();
    };
}
